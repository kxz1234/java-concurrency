package com.colin.juc.threadlocal;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Colin on 2020/4/5 13:20
 * email: colinzhaodong@gmail.com
 * desc: //两个线程打印日期没有问题
 *
 * @author zhaod
 */
public class ThreadLocalUsageOne {
	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String date = new ThreadLocalUsageOne().date(10);
				System.out.println(date);
			}
		}).start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				String date = new ThreadLocalUsageOne().date(1007000000L);
				System.out.println(date);
			}
		}).start();
	}

	public String date(long seconds){
		//参数的单位是毫秒，从1970.1.1 00:00:00 GMT计时
		Date date = new Date(1000 * seconds);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return simpleDateFormat.format(date);
	}
}
