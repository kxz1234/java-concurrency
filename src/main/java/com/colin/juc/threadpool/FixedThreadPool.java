package com.colin.juc.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Colin on 2020/4/4 13:23
 * email: colinzhaodong@gmail.com
 * desc: 演示newFixedThreadPool执行任务
 *
 * @author zhaod
 */
public class FixedThreadPool {
	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(4);
		for (int i = 0; i < 10; i++) {
			executorService.execute(new Task());
		}
	}
}
class Task implements Runnable{

	@Override
	public void run() {
		try {
			TimeUnit.MILLISECONDS.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName());
	}
}
