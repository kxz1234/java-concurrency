package com.colin.juc.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Colin on 2020/4/4 13:53
 * email: colinzhaodong@gmail.com
 * desc: 单独线程
 *
 * @author zhaod
 */
public class SingleThreadExecutor {
	public static void main(String[] args) {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		for (int i = 0; i < 10; i++) {
			executorService.execute(new Task());
		}
	}
}
