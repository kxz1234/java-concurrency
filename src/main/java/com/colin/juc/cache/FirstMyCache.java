package com.colin.juc.cache;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Colin on 2020/11/21 17:16
 * email: colinzhaodong@gmail.com
 * desc:
 *
 * @author zhaod
 */
public class FirstMyCache {
	private final HashMap<String, Integer> cache = new HashMap<>();

	public synchronized Integer computer(String userId) throws InterruptedException {
		Integer result = cache.get(userId);
		//先检查HashMap里面有没有保存过之前的计算结果
		if (result == null) {
			//如果缓存中找不到，那么需要现在计算一下结果，并且保存到HashMap中
			result = doCompute(userId);
			cache.put(userId, result);
		}
		return result;
	}

	private Integer doCompute(String userId) throws InterruptedException {
		TimeUnit.SECONDS.sleep(5);
		return new Integer(userId);
	}

	public static void main(String[] args) throws InterruptedException {
		FirstMyCache firstMyCache = new FirstMyCache();
		System.out.println("开始计算了");
		Integer result = firstMyCache.computer("13");
		System.out.println("第一次计算结果：" + result);
		result = firstMyCache.computer("13");
		System.out.println("第二次计算结果：" + result);

	}
}
