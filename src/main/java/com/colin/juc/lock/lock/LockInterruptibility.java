package com.colin.juc.lock.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Colin on 2020/4/7 20:17
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class LockInterruptibility implements Runnable{
	private Lock lock = new ReentrantLock();

	public static void main(String[] args) throws InterruptedException {
		LockInterruptibility lockInterruptibility = new LockInterruptibility();
		Thread thread0 = new Thread(lockInterruptibility);
		Thread thread1 = new Thread(lockInterruptibility);
		thread0.start();
//		thread0.join();
		thread1.start();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread1.interrupt();
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName() + "尝试获取锁");
		try {
			lock.lockInterruptibly();
			try {
				System.out.println(Thread.currentThread().getName() + "获取到了锁");
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				System.out.println(Thread.currentThread().getName() + "睡眠期间被中断了");
			} finally {
				lock.unlock();
				System.out.println(Thread.currentThread().getName() + "释放了锁");
			}
		} catch (InterruptedException e) {
			System.out.println(Thread.currentThread().getName() + "获得锁期间被中断了");
		}
	}


}
