package com.colin.base.background;

/**
 * Created by Colin on 2020/3/14 15:01
 * email: colinzhaodong@gmail.com
 * desc: 计算不准确
 *
 * @author zhaod
 */
public class MultiThreadError implements Runnable{
	int index = 0;
	static MultiThreadError instance = null;
	static {
		instance = new MultiThreadError();
	}
//	final boolean[] marked = new boolean[1000000];
	public static void main(String[] args) throws InterruptedException {
		Thread thread1 = new Thread(instance);
		Thread thread2 = new Thread(instance);
		thread1.start();
		thread2.start();
		thread1.join();
		thread2.join();
		System.out.println(instance.index);
	}
	@Override
	public void run() {
		for (int i = 0; i < 10000; i++) {
			index++;
//			marked[index] = true;
		}
	}

//	public synchronized void count(){
//		for (int i = 0; i < 10000; i++) {
//			index++;
//		}
//	}
}
