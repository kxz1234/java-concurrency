package com.colin.base.background;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Colin on 2020/3/14 15:52
 * email: colinzhaodong@gmail.com
 * desc: 对象发布溢出
 *
 * @author zhaod
 */
public class PublicAndOverflow {
	private final Map<String, String> states;

	public PublicAndOverflow() {
		states = new HashMap<>();
		states.put("1", "周一");
		states.put("2", "周二");
		states.put("3", "周三");
		states.put("4", "周四");
	}

	public Map<String, String> getStates() {
		return states;
	}

	public Map<String, String> getStatesImproved() {
		return new HashMap<>(states);
	}

	public static void main(String[] args) {
		PublicAndOverflow multiThreadsError3 = new PublicAndOverflow();
//		Map<String, String> states = multiThreadsError3.getStatesImproved();
        System.out.println(multiThreadsError3.getStatesImproved().get("1"));
		multiThreadsError3.getStatesImproved().remove("1");
        System.out.println(multiThreadsError3.getStatesImproved().get("1"));

//		System.out.println(multiThreadsError3.getStatesImproved().get("1"));
//		multiThreadsError3.getStatesImproved().remove("1");
//		System.out.println(multiThreadsError3.getStatesImproved().get("1"));

	}
}
