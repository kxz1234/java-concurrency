package com.colin.base.background;

/**
 * Created by Colin on 2020/3/14 15:58
 * email: colinzhaodong@gmail.com
 * desc: 对象溢出在初始化的时候
 *
 * @author zhaod
 */
public class OverflowInInitial {
	static Point point;

	public static void main(String[] args) throws InterruptedException {
		new PointMaker().start();
//        Thread.sleep(10);
		Thread.sleep(105);
		if (point != null) {
			System.out.println(point);
		}
	}
}

class Point {

	private final int x, y;

	public Point(int x, int y) throws InterruptedException {
		this.x = x;
		OverflowInInitial.point = this;
		Thread.sleep(100);
		this.y = y;
	}

	@Override
	public String toString() {
		return x + "," + y;
	}
}

class PointMaker extends Thread {

	@Override
	public void run() {
		try {
			new Point(1, 1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
