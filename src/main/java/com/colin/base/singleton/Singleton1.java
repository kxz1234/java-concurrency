package com.colin.base.singleton;

/**
 * Created by Colin on 2020/3/21 14:34
 * email: colinzhaodong@gmail.com
 * desc: 饿汉式（静态常量）（可用）
 *
 * @author zhaod
 */
public class Singleton1 {
	private final static Singleton1 INSTANCE = new Singleton1();
	private Singleton1(){

	}
	public static Singleton1 getInstance(){
		return INSTANCE;
	}
}
