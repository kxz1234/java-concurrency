package com.colin.base.javaconcurrency.stopthread;

/**
 * @author: BlueMelancholy
 * 2019/12/17 14:14
 * @desc 每次循环都会调用sleep或wait等方法   这种情况每次迭代不需要去检查是否已中断,因为每次sleep时,去中断,
 * 会收到异常形式的中断响应
 * @email zhaod@oceansoft.com.cn
 */
public class RightWaysStopThreadaWithSleepEveryLoop {
    public static void main(String[] args) throws InterruptedException {
        Runnable runnable = () -> {
            int num = 0;
            try {
                while (num <= 10000){
                    if (num % 100 == 0){
                        System.out.println(num + "是100的倍数");
                    }
                    num++;
                    Thread.sleep(10);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        Thread.sleep(5000);
        thread.interrupt();
    }
}
