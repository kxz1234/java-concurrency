package com.colin.base.javaconcurrency.sixstates;

/**
 * Created by Colin on 2020/3/3 20:01
 * email: colinzhaodong@gmail.com
 * desc: 描述：     展示线程的NEW、RUNNABLE、Terminated状态。即使是正在运行，也是Runnable状态，而不是Running。
 *
 * @author zhaod
 */
public class NewRunnableTerminated implements Runnable{
	public static void main(String[] args) {
		Thread thread = new Thread(new NewRunnableTerminated());
		System.out.println(thread.getState());
		thread.start();
		System.out.println(thread.getState());
		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(thread.getState());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(thread.getState());
	}

	@Override
	public void run() {
		for (int i = 0; i <1000 ; i++) {
			System.out.println(i);
		}
	}
}
