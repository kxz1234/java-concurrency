package com.colin.functioninterface;

/**
 * Created by Colin on 2020/4/7 15:31
 * email: colinzhaodong@gmail.com
 * desc: 函数式接口lambda表达式实现
 *
 * @author zhaod
 */
public class Test1 {
	public static void main(String[] args) {
		MyFirstFunctionInterface.NoReturnNoParam noReturnNoParam = () -> {
			System.out.println("NoReturnNoParam");
		};
		noReturnNoParam.method();
		MyFirstFunctionInterface.NoReturnOneParam noReturnOneParam = (int a) -> {
			System.out.println("NoReturnOneParam param:" + a);
		};
		noReturnOneParam.method(2);

		//多个参数无返回
		MyFirstFunctionInterface.NoReturnMultiParam noReturnMultiParam = (int a, int b) -> {
			System.out.println("NoReturnMultiParam param:" + "{" + a +"," + + b +"}");
		};
		noReturnMultiParam.method(6, 8);

		//无参有返回值
		MyFirstFunctionInterface.ReturnNoParam returnNoParam = () -> {
			System.out.print("ReturnNoParam");
			return 1;
		};

		int res = returnNoParam.method();
		System.out.println("return:" + res);

		//一个参数有返回值
		MyFirstFunctionInterface.ReturnOneParam returnOneParam = (int a) -> {
			System.out.println("ReturnOneParam param:" + a);
			return 1;
		};

		int res2 = returnOneParam.method(6);
		System.out.println("return:" + res2);

		//多个参数有返回值
		MyFirstFunctionInterface.ReturnMultiParam returnMultiParam = (int a, int b) -> {
			System.out.println("ReturnMultiParam param:" + "{" + a + "," + b +"}");
			return 1;
		};

		int res3 = returnMultiParam.method(6, 8);
		System.out.println("return:" + res3);
	}
}
