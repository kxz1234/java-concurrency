package com.colin.base.javaconcurrency;

/**
 * Created by saintess on 2017/2/9.
 */
public class ManifestTask {
    //附件guid
    private  String guid;
    //办件材料ID
    private  String matMId;

    private  String fileSize;

    //文件真实名称
    private  String fileRealName;

    private String uid;

    private  String url;

    private  String isNew;

    private String fileMaterialName;

    private String attUrl;

    public String getFileMaterialName() {
        return fileMaterialName;
    }

    public void setFileMaterialName(String fileMaterialName) {
        this.fileMaterialName = fileMaterialName;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileRealName() {
        return fileRealName;
    }

    public void setFileRealName(String fileRealName) {
        this.fileRealName = fileRealName;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getMatMId() {
        return matMId;
    }

    public void setMatMId(String matMId) {
        this.matMId = matMId;
    }

    public String getAttUrl() {
        return attUrl;
    }

    public void setAttUrl(String attUrl) {
        this.attUrl = attUrl;
    }

    @Override
    public String toString() {
        return "ManifestTask{" +
                "guid='" + guid + '\'' +
                ", matMId='" + matMId + '\'' +
                ", fileSize='" + fileSize + '\'' +
                ", fileRealName='" + fileRealName + '\'' +
                ", uid='" + uid + '\'' +
                ", url='" + url + '\'' +
                ", isNew='" + isNew + '\'' +
                ", fileMaterialName='" + fileMaterialName + '\'' +
                ", attUrl='" + attUrl + '\'' +
                '}';
    }
}
