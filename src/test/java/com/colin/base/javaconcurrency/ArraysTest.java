package com.colin.base.javaconcurrency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Colin on 2020/6/12 15:32
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class ArraysTest {

	public static void main(String[] args) {
		//String[] auditArray = {"1","2","3"};
		//List<String> strings = new ArrayList<>();
		//strings.add("1");
		//strings.add("5");
		//strings.add("9");
		//strings.add("13");
		//strings.add("2");
		//for (String string : strings) {
		//	System.out.println(Arrays.binarySearch(auditArray,string));
		//}
		ManifestTask manifestTask = new ManifestTask();
		manifestTask.setIsNew("1");
		List<ManifestTask> manifestTaskList = new ArrayList<>();
		manifestTaskList.add(manifestTask);
		CopyOnWriteArrayList<ManifestTask> concurrentInternetList = new CopyOnWriteArrayList<>(manifestTaskList);
		concurrentInternetList.forEach(item -> {
			//2代表已经确认上传
			item.setIsNew("2");
		});
		for (ManifestTask task : manifestTaskList) {
			System.out.println(task);
		}
	}
}
