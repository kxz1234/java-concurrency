package com.colin.base.javaconcurrency;

import com.colin.base.javaconcurrency.VO.AnalysisResultVO;
import com.colin.base.javaconcurrency.VO.DuplicateNameVO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by Colin on 2020/6/20 15:48
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class CountIdNumTest {
	/**
	 * 水瓶座人数
	 */
	private final AtomicInteger aquariusLongAddr = new AtomicInteger();
	/**
	 * 双鱼座人数
	 */
	private final AtomicInteger piscesAddr = new AtomicInteger();
	/**
	 * 白羊座人数
	 */
	private final AtomicInteger ariesAddr = new AtomicInteger();
	/**
	 * 金牛座人数
	 */
	private final AtomicInteger taurusAddr = new AtomicInteger();
	/**
	 * 双子座人数
	 */
	private final AtomicInteger geminiAddr = new AtomicInteger();
	/**
	 * 巨蟹座人数
	 */
	private final AtomicInteger cancerAddr = new AtomicInteger();
	/**
	 * 狮子座人数
	 */
	private final AtomicInteger leoAddr = new AtomicInteger();
	/**
	 * 处女座人数
	 */
	private final AtomicInteger virgoAddr = new AtomicInteger();
	/**
	 * 天秤座人数
	 */
	private final AtomicInteger libraAddr = new AtomicInteger();
	/**
	 * 天蝎座人数
	 */
	private final AtomicInteger scorpioAddr = new AtomicInteger();
	/**
	 * 射手座人数
	 */
	private final AtomicInteger sagittariusAddr = new AtomicInteger();
	/**
	 * 摩羯座人数
	 */
	private final AtomicInteger capricornAddr = new AtomicInteger();

	/**
	 * 3岁以上
	 */
	private final AtomicInteger beyondThreeAddr = new AtomicInteger();
	/**
	 * 3至6岁
	 */
	private final AtomicInteger threeToSixAddr = new AtomicInteger();
	/**
	 * 6-20岁人数
	 */
	private final AtomicInteger sixToTwentyAddr = new AtomicInteger();
	/**
	 * 20-60岁人数
	 */
	private final AtomicInteger twentyToSixtyAddr = new AtomicInteger();

	/**
	 * 50-60岁人数
	 */
	private final AtomicInteger overSixtyAddr = new AtomicInteger();

	/**
	 * 男
	 */
	private final AtomicInteger maleNumAddr = new AtomicInteger();
	/**
	 * 女
	 */
	private final AtomicInteger femaleNumAddr = new AtomicInteger();


	public DuplicateNameVO countQuantity(final Set<String> idNumSet){
		for (String idNum : idNumSet) {
			AnalysisResultVO analysisResultVO = AnalyzeIdNumUtil.composeAnalysisResult(idNum);
			if (analysisResultVO.getAge() < 3){
				beyondThreeAddr.incrementAndGet();
			}else if (analysisResultVO.getAge() >=3 && analysisResultVO.getAge() < 6){
				threeToSixAddr.incrementAndGet();
			}else if (analysisResultVO.getAge() >= 6 && analysisResultVO.getAge() <20){
				sixToTwentyAddr.incrementAndGet();
			}else if (analysisResultVO.getAge() >= 20 && analysisResultVO.getAge() < 60){
				twentyToSixtyAddr.incrementAndGet();
			}else if (analysisResultVO.getAge() >= 60){
				overSixtyAddr.incrementAndGet();
			}
			if (analysisResultVO.getSex() == 1){
				maleNumAddr.incrementAndGet();
			}else if (analysisResultVO.getSex() == 0){
				femaleNumAddr.incrementAndGet();
			}
			switch (analysisResultVO.getConstellation()){

				case 1 : aquariusLongAddr.incrementAndGet();break;
				case 2 : piscesAddr.incrementAndGet();break;
				case 3 : ariesAddr.incrementAndGet();break;
				case 4 : taurusAddr.incrementAndGet();break;
				case 5 : geminiAddr.incrementAndGet();break;
				case 6 : cancerAddr.incrementAndGet();break;
				case 7 : leoAddr.incrementAndGet();break;
				case 8 : virgoAddr.incrementAndGet();break;
				case 9 : libraAddr.incrementAndGet();break;
				case 10 : scorpioAddr.incrementAndGet();break;
				case 11 : sagittariusAddr.incrementAndGet();break;
				case 12 : capricornAddr.incrementAndGet();break;
				default: ;
			}
		}
		return DuplicateNameVO.create()
				.count(idNumSet.size())
				.beyondThree((beyondThreeAddr.intValue()))
				.threeToSix(threeToSixAddr.intValue())
				.sixToTwenty(sixToTwentyAddr.intValue())
				.twentyToSixty(twentyToSixtyAddr.intValue())
				.overSixty(overSixtyAddr.intValue())
				.maleNum(maleNumAddr.intValue())
				.femaleNum(femaleNumAddr.intValue())
				.aquariusStar(aquariusLongAddr.intValue())
				.piscesStar(piscesAddr.intValue())
				.ariesStar(ariesAddr.intValue())
				.taurusStar(taurusAddr.intValue())
				.geminiStar(geminiAddr.intValue())
				.cancerStar(cancerAddr.intValue())
				.leoStar(leoAddr.intValue())
				.virgoStar(virgoAddr.intValue())
				.libraStar(libraAddr.intValue())
				.scorpioStar(scorpioAddr.intValue())
				.sagittariusStar(sagittariusAddr.intValue())
				.capricornStar(capricornAddr.intValue())
				.build();
	}

	public static void main(String[] args) {
		Set<String> idNumSet = new HashSet<>();
		idNumSet.add("50022620111213223X");
		idNumSet.add("511302198910270736");
		idNumSet.add("142326198402121219");
		idNumSet.add("320682198704033897");
		idNumSet.add("500243200102103955");
		idNumSet.add("500240199011184097");
		idNumSet.add("370782199305073671");
		idNumSet.add("130181198203246115");
		idNumSet.add("500113199601145536");
		idNumSet.add("370921199011140056");
		idNumSet.add("500222200212235412");
		idNumSet.add("512301197210207090");
		idNumSet.add("61020319840605291X");
		idNumSet.add("140524199003170014");
		idNumSet.add("500243201503133519");
		idNumSet.add("500236199211121731");
		idNumSet.add("500110199305132432");
		CountIdNumTest countIdNumTest = new CountIdNumTest();
		System.out.println(countIdNumTest.countQuantity(idNumSet));
	}
}
