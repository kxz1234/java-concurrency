package com.colin.base.javaconcurrency.VO;

/**
 * Created by Colin on 2020/6/18 10:30
 * email: colinzhaodong@gmail.com
 * desc: 重名查询VO返回对象
 *
 * @author zhaod
 */
public class DuplicateNameVO {
	/**重名人数**/
	private Integer count;

	/**
	 * 水瓶座人数
	 */
	private Integer aquariusStar;
	/**
	 * 双鱼座人数
	 */
	private Integer piscesStar;
	/**
	 * 白羊座人数
	 */
	private Integer ariesStar;
	/**
	 * 金牛座人数
	 */
	private Integer taurusStar;
	/**
	 * 双子座人数
	 */
	private Integer geminiStar;
	/**
	 * 巨蟹座人数
	 */
	private Integer cancerStar;
	/**
	 * 狮子座人数
	 */
	private Integer leoStar;
	/**
	 * 处女座人数
	 */
	private Integer virgoStar;
	/**
	 * 天秤座人数
	 */
	private Integer libraStar;
	/**
	 * 天蝎座人数
	 */
	private Integer scorpioStar;
	/**
	 * 射手座人数
	 */
	private Integer sagittariusStar;
	/**
	 * 摩羯座人数
	 */
	private Integer capricornStar;

	/**
	 * 3岁以上
	 */
	private Integer beyondThree;
	/**
	 * 3至6岁
	 */
	private Integer threeToSix;
	/**
	 * 6-20岁人数
	 */
	private Integer sixToTwenty;
	/**
	 * 20-60岁人数
	 */
	private Integer twentyToSixty;

	/**
	 * 50-60岁人数
	 */
	private Integer overSixty;

	/**
	 * 男
	 */
	private Integer maleNum;
	/**
	 * 女
	 */
	private Integer femaleNum;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getAquariusStar() {
		return aquariusStar;
	}

	public void setAquariusStar(Integer aquariusStar) {
		this.aquariusStar = aquariusStar;
	}

	public Integer getPiscesStar() {
		return piscesStar;
	}

	public void setPiscesStar(Integer piscesStar) {
		this.piscesStar = piscesStar;
	}

	public Integer getAriesStar() {
		return ariesStar;
	}

	public void setAriesStar(Integer ariesStar) {
		this.ariesStar = ariesStar;
	}

	public Integer getTaurusStar() {
		return taurusStar;
	}

	public void setTaurusStar(Integer taurusStar) {
		this.taurusStar = taurusStar;
	}

	public Integer getGeminiStar() {
		return geminiStar;
	}

	public void setGeminiStar(Integer geminiStar) {
		this.geminiStar = geminiStar;
	}

	public Integer getCancerStar() {
		return cancerStar;
	}

	public void setCancerStar(Integer cancerStar) {
		this.cancerStar = cancerStar;
	}

	public Integer getLeoStar() {
		return leoStar;
	}

	public void setLeoStar(Integer leoStar) {
		this.leoStar = leoStar;
	}

	public Integer getVirgoStar() {
		return virgoStar;
	}

	public void setVirgoStar(Integer virgoStar) {
		this.virgoStar = virgoStar;
	}

	public Integer getLibraStar() {
		return libraStar;
	}

	public void setLibraStar(Integer libraStar) {
		this.libraStar = libraStar;
	}

	public Integer getScorpioStar() {
		return scorpioStar;
	}

	public void setScorpioStar(Integer scorpioStar) {
		this.scorpioStar = scorpioStar;
	}

	public Integer getSagittariusStar() {
		return sagittariusStar;
	}

	public void setSagittariusStar(Integer sagittariusStar) {
		this.sagittariusStar = sagittariusStar;
	}

	public Integer getCapricornStar() {
		return capricornStar;
	}

	public void setCapricornStar(Integer capricornStar) {
		this.capricornStar = capricornStar;
	}

	public Integer getBeyondThree() {
		return beyondThree;
	}

	public void setBeyondThree(Integer beyondThree) {
		this.beyondThree = beyondThree;
	}

	public Integer getThreeToSix() {
		return threeToSix;
	}

	public void setThreeToSix(Integer threeToSix) {
		this.threeToSix = threeToSix;
	}

	public Integer getSixToTwenty() {
		return sixToTwenty;
	}

	public void setSixToTwenty(Integer sixToTwenty) {
		this.sixToTwenty = sixToTwenty;
	}

	public Integer getTwentyToSixty() {
		return twentyToSixty;
	}

	public void setTwentyToSixty(Integer twentyToSixty) {
		this.twentyToSixty = twentyToSixty;
	}

	public Integer getOverSixty() {
		return overSixty;
	}

	public void setOverSixty(Integer overSixty) {
		this.overSixty = overSixty;
	}

	public Integer getMaleNum() {
		return maleNum;
	}

	public void setMaleNum(Integer maleNum) {
		this.maleNum = maleNum;
	}

	public Integer getFemaleNum() {
		return femaleNum;
	}

	public void setFemaleNum(Integer femaleNum) {
		this.femaleNum = femaleNum;
	}

	public static DuplicateNameVOBuilder create(){
		return new DuplicateNameVOBuilder();
	}


	public static final class DuplicateNameVOBuilder {
		private Integer count;
		private Integer aquariusStar;
		private Integer piscesStar;
		private Integer ariesStar;
		private Integer taurusStar;
		private Integer geminiStar;
		private Integer cancerStar;
		private Integer leoStar;
		private Integer virgoStar;
		private Integer libraStar;
		private Integer scorpioStar;
		private Integer sagittariusStar;
		private Integer capricornStar;
		private Integer beyondThree;
		private Integer threeToSix;
		private Integer sixToTwenty;
		private Integer twentyToSixty;
		private Integer overSixty;
		private Integer maleNum;
		private Integer femaleNum;

		private DuplicateNameVOBuilder() {
		}

		public static DuplicateNameVOBuilder aDuplicateNameVO() {
			return new DuplicateNameVOBuilder();
		}

		public DuplicateNameVOBuilder count(Integer count) {
			this.count = count;
			return this;
		}

		public DuplicateNameVOBuilder aquariusStar(Integer aquariusStar) {
			this.aquariusStar = aquariusStar;
			return this;
		}

		public DuplicateNameVOBuilder piscesStar(Integer piscesStar) {
			this.piscesStar = piscesStar;
			return this;
		}

		public DuplicateNameVOBuilder ariesStar(Integer ariesStar) {
			this.ariesStar = ariesStar;
			return this;
		}

		public DuplicateNameVOBuilder taurusStar(Integer taurusStar) {
			this.taurusStar = taurusStar;
			return this;
		}

		public DuplicateNameVOBuilder geminiStar(Integer geminiStar) {
			this.geminiStar = geminiStar;
			return this;
		}

		public DuplicateNameVOBuilder cancerStar(Integer cancerStar) {
			this.cancerStar = cancerStar;
			return this;
		}

		public DuplicateNameVOBuilder leoStar(Integer leoStar) {
			this.leoStar = leoStar;
			return this;
		}

		public DuplicateNameVOBuilder virgoStar(Integer virgoStar) {
			this.virgoStar = virgoStar;
			return this;
		}

		public DuplicateNameVOBuilder libraStar(Integer libraStar) {
			this.libraStar = libraStar;
			return this;
		}

		public DuplicateNameVOBuilder scorpioStar(Integer scorpioStar) {
			this.scorpioStar = scorpioStar;
			return this;
		}

		public DuplicateNameVOBuilder sagittariusStar(Integer sagittariusStar) {
			this.sagittariusStar = sagittariusStar;
			return this;
		}

		public DuplicateNameVOBuilder capricornStar(Integer capricornStar) {
			this.capricornStar = capricornStar;
			return this;
		}

		public DuplicateNameVOBuilder beyondThree(Integer beyondThree) {
			this.beyondThree = beyondThree;
			return this;
		}

		public DuplicateNameVOBuilder threeToSix(Integer threeToSix) {
			this.threeToSix = threeToSix;
			return this;
		}

		public DuplicateNameVOBuilder sixToTwenty(Integer sixToTwenty) {
			this.sixToTwenty = sixToTwenty;
			return this;
		}

		public DuplicateNameVOBuilder twentyToSixty(Integer twentyToSixty) {
			this.twentyToSixty = twentyToSixty;
			return this;
		}

		public DuplicateNameVOBuilder overSixty(Integer overSixty) {
			this.overSixty = overSixty;
			return this;
		}

		public DuplicateNameVOBuilder maleNum(Integer maleNum) {
			this.maleNum = maleNum;
			return this;
		}

		public DuplicateNameVOBuilder femaleNum(Integer femaleNum) {
			this.femaleNum = femaleNum;
			return this;
		}

		public DuplicateNameVO build() {
			DuplicateNameVO duplicateNameVO = new DuplicateNameVO();
			duplicateNameVO.setCount(count);
			duplicateNameVO.setAquariusStar(aquariusStar);
			duplicateNameVO.setPiscesStar(piscesStar);
			duplicateNameVO.setAriesStar(ariesStar);
			duplicateNameVO.setTaurusStar(taurusStar);
			duplicateNameVO.setGeminiStar(geminiStar);
			duplicateNameVO.setCancerStar(cancerStar);
			duplicateNameVO.setLeoStar(leoStar);
			duplicateNameVO.setVirgoStar(virgoStar);
			duplicateNameVO.setLibraStar(libraStar);
			duplicateNameVO.setScorpioStar(scorpioStar);
			duplicateNameVO.setSagittariusStar(sagittariusStar);
			duplicateNameVO.setCapricornStar(capricornStar);
			duplicateNameVO.setBeyondThree(beyondThree);
			duplicateNameVO.setThreeToSix(threeToSix);
			duplicateNameVO.setSixToTwenty(sixToTwenty);
			duplicateNameVO.setTwentyToSixty(twentyToSixty);
			duplicateNameVO.setOverSixty(overSixty);
			duplicateNameVO.setMaleNum(maleNum);
			duplicateNameVO.setFemaleNum(femaleNum);
			return duplicateNameVO;
		}
	}

	@Override
	public String toString() {
		return "DuplicateNameVO{" +
				"count=" + count +
				", aquariusStar=" + aquariusStar +
				", piscesStar=" + piscesStar +
				", ariesStar=" + ariesStar +
				", taurusStar=" + taurusStar +
				", geminiStar=" + geminiStar +
				", cancerStar=" + cancerStar +
				", leoStar=" + leoStar +
				", virgoStar=" + virgoStar +
				", libraStar=" + libraStar +
				", scorpioStar=" + scorpioStar +
				", sagittariusStar=" + sagittariusStar +
				", capricornStar=" + capricornStar +
				", beyondThree=" + beyondThree +
				", threeToSix=" + threeToSix +
				", sixToTwenty=" + sixToTwenty +
				", twentyToSixty=" + twentyToSixty +
				", overSixty=" + overSixty +
				", maleNum=" + maleNum +
				", femaleNum=" + femaleNum +
				'}';
	}
}
